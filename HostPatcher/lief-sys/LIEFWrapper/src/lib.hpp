#include "LIEF/LIEF.hpp"

extern "C" {
  LIEF::ELF::Binary* LIEF_ELF_Parser_parse(const uint8_t* data, const size_t dataLength, const char* filename, const int mode);
  void LIEF_ELF_Binary_add_library(LIEF::ELF::Binary* binary, const char* filename);
  uint8_t* LIEF_ELF_Binary_raw(LIEF::ELF::Binary* binary, size_t* length);
  void LIEF_free(void* ptr);
  void LIEF_ELF_Binary_delete(LIEF::ELF::Binary* binary);
}
