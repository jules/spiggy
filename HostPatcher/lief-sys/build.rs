use cmake;
use std::env;
use std::path::Path;
use std::process::Command;

fn main() {
  let target = env::var("TARGET").unwrap();

  if !Path::new("LIEFWrapper/LIEF/.git").exists() {
    let _ = Command::new("git")
      .args(&["submodule", "update", "--init"])
      .status();
  }

  let dst = cmake::build("LIEFWrapper");

  println!(
    "cargo:rustc-link-search=native={}",
    dst.join("lib").display()
  );
  println!("cargo:rustc-link-lib=static=LIEFWrapper");

  if target.contains("linux") {
    println!("cargo:rustc-link-lib=dylib=stdc++");
  } else if target.contains("apple") {
    println!("cargo:rustc-link-lib=dylib=c++");
  }
}
