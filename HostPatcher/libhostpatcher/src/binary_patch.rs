use lief_sys;
use std::error::Error;
use std::io::Read;

pub fn inject_binary(mut patchable: impl Read) -> Result<Vec<u8>, Box<dyn Error + Send + Sync>> {
  let mut patchable_bytes: Vec<u8> = Vec::new();
  patchable.read_to_end(&mut patchable_bytes)?;
  let elf = lief_sys::ELFBinary::new(patchable_bytes, "libdsti.so".to_string()).unwrap();
  Ok(elf.add_library("libgadget.so".to_string()).raw())
}
