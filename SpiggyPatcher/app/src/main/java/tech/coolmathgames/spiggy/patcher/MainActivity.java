package tech.coolmathgames.spiggy.patcher;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.io.IOException;

import tech.coolmathgames.spiggy.patcher.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

  private AppBarConfiguration appBarConfiguration;
  private ActivityMainBinding binding;
  private String requestingPackageName;
  private final ActivityResultLauncher<String> getContent = registerForActivityResult(new ActivityResultContracts.GetContent(), result -> {
    if (result == null) return;
    Runnable runnable = () -> {
      Bitmap bitmap;
      try {
        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), result);
      } catch (IOException e) {
        e.printStackTrace();
        return;
      }
      SpiggyApp spiggyApp = (SpiggyApp) getApplication();
      StorageController sc = spiggyApp.getStorageController();
      for (PatchedApp patchedApp : sc.getApps()) {
        if (patchedApp.getPackageName().equals(requestingPackageName)) {
          try {
            patchedApp.setIcon(bitmap);
          } catch (IOException e) {
            e.printStackTrace();
          }
          break;
        }
      }
    };
    new Thread(runnable).start();
  });

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    SpiggyApp app = (SpiggyApp) this.getApplication();
    app.setActivity(this);

    binding = ActivityMainBinding.inflate(getLayoutInflater());
    setContentView(binding.getRoot());

    setSupportActionBar(binding.toolbar);

    NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
    appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
    NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      binding.fab.setOnClickListener(view -> {
        StorageController sc = app.getStorageController();
        sc.updateBranches();
        for (PatchedApp patchedApp : sc.getApps()) {
          patchedApp.patch();
        }
      });
    }
  }

  @Override
  public boolean onCreateOptionsMenu(@NonNull Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  public boolean onSupportNavigateUp() {
    NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
    return NavigationUI.navigateUp(navController, appBarConfiguration)
        || super.onSupportNavigateUp();
  }

  @Override
  public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
    super.onSaveInstanceState(savedInstanceState);
    savedInstanceState.putString("requestingPackageName", this.requestingPackageName);
  }

  @Override
  protected void onRestoreInstanceState(Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
    this.requestingPackageName = savedInstanceState.getString("requestingPackageName");
  }

  public void selectImage(PatchedApp patchedApp) {
    this.requestingPackageName = patchedApp.getPackageName();
    this.getContent.launch("image/*");
  }

  public void syncApps() {
    Fragment fragmentMain = this.getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment_content_main);
    if (fragmentMain != null) {
      FragmentManager manager = fragmentMain.getChildFragmentManager();
      for (Fragment fragment : manager.getFragments()) {
        if (fragment instanceof RefreshableFragment) {
          ((RefreshableFragment) fragment).refreshApps();
        }
      }
    }
  }
}