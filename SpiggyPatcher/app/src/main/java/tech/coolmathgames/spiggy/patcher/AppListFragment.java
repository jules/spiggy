package tech.coolmathgames.spiggy.patcher;

import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import java.util.List;
import java.util.Objects;

import tech.coolmathgames.spiggy.patcher.databinding.FragmentAppListBinding;

public class AppListFragment extends Fragment implements RefreshableFragment {

  private FragmentAppListBinding binding;
  private static final ColorDrawable DISCORD_BACKGROUND = new ColorDrawable(0xfffaa61a);

  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState
  ) {

    binding = FragmentAppListBinding.inflate(inflater, container, false);
    return binding.getRoot();

  }

  public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    binding.buttonCreate.setOnClickListener(view13 -> NavHostFragment.findNavController(this)
        .navigate(R.id.action_create_app));

    this.inflateAppList();
  }

  public void refreshApps() {
    this.inflateAppList();
  }

  private void inflateAppList() {
    System.out.println("Inflating app list!");
    SpiggyApp spiggyApp = (SpiggyApp) this.requireActivity().getApplication();
    List<PatchedApp> apps = spiggyApp.getStorageController().getApps();
    this.requireActivity().runOnUiThread(() -> {
      binding.appList.removeAllViews();
      for (PatchedApp patchedApp : apps) {
        View appButton = View.inflate(this.getContext(), R.layout.app_button, null);
        binding.appList.addView(appButton);
        TextView appName = appButton.findViewById(R.id.appName);
        appName.setText(patchedApp.getPackageName());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          appButton.setOnClickListener(view1 -> {
            spiggyApp.getStorageController().updateBranch(patchedApp.getBranch());
            patchedApp.patch();
          });
        }
        ImageView appIcon = appButton.findViewById(R.id.appIcon);
        if (patchedApp.getHasIcon()) {
          Uri uri;
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            uri = FileProvider.getUriForFile(
                spiggyApp,
                BuildConfig.APPLICATION_ID + ".fileprovider",
                patchedApp.getIconPath()
            );
          } else {
            uri = Uri.fromFile(patchedApp.getIconPath());
          }
          if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            Drawable foreground = Drawable.createFromPath(patchedApp.getIconPath().getAbsolutePath());
            AdaptiveIconDrawable adaptiveIcon = new AdaptiveIconDrawable(DISCORD_BACKGROUND, foreground);
            appIcon.setImageDrawable(adaptiveIcon);
          } else {
            appIcon.setImageURI(uri);
          }
        }
        appIcon.setOnClickListener(view12 -> Objects.requireNonNull(spiggyApp.getActivity()).selectImage(patchedApp));
        appButton.setOnLongClickListener(view12 -> {
          patchedApp.delete();
          return true;
        });
      }
    });
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    binding = null;
  }

}