package tech.coolmathgames.spiggy.patcher;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;

import com.android.apksig.ApkSigner;
import com.android.apksig.util.DataSources;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PatchedApp {
  private final PatchedAppData data;
  private final StorageController storageController;

  public PatchedApp(PatchedAppData data, StorageController storageController) {
    this.data = data;
    this.storageController = storageController;
  }

  public String getPackageName() {
    return this.data.getPackageName();
  }

  public String getDisplayName() {
    return this.data.getDisplayName();
  }

  public String getBranch() {
    return this.data.getBranch();
  }

  public void setBranch(String name) {
    this.data.setBranch(name);
    this.sync();
  }

  public void setIcon(@Nullable Bitmap bitmap) throws IOException {
    this.storageController.getIconFolder().mkdirs();
    File outputPath = this.getIconPath();
    if (bitmap != null) {
      OutputStream output = new FileOutputStream(outputPath);
      if (bitmap.getHeight() != bitmap.getWidth()) {
        int size = Math.min(bitmap.getHeight(), bitmap.getWidth());
        bitmap = Bitmap.createBitmap(
            bitmap,
            (bitmap.getWidth()  - size) / 2,
            (bitmap.getHeight() - size) / 2,
            size,
            size
        );
      }
      bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
      output.close();
    } else {
      outputPath.delete();
    }
    this.data.setHasIcon(bitmap != null);
    this.sync();
  }

  public File getIconPath() {
    return new File(this.storageController.getIconFolder(), this.getPackageName() + ".png");
  }

  public PatchedAppData getData() {
    return this.data;
  }

  private void sync() {
    this.storageController.syncApps();
  }

  public void patch() {
    SpiggyApp spiggyApp = this.storageController.getSpiggyApp();
    Runnable runnable = () -> {
      ApplicationInfo applicationInfo = getDiscordAppInfo();
      if (applicationInfo == null) {
        throw new Error("Missing com.discord!");
      }

      String iconPath = null;
      if (this.getHasIcon()) {
        iconPath = this.getIconPath().getAbsolutePath();
      }

      File outputDir = spiggyApp.getCacheDir(); // context being the Activity pointer
      File signedDir = new File(outputDir, "signed");
      signedDir.mkdirs();
      File tempSignedAPK;
      try {
        tempSignedAPK = File.createTempFile(
            "spiggy-signed", ".apk", signedDir
        );
      } catch (IOException e) {
        e.printStackTrace();
        return;
      }
      tempSignedAPK.deleteOnExit();
      File tempAPK;
      try {
        tempAPK = File.createTempFile("spiggy", ".apk", outputDir);
      } catch (IOException e) {
        e.printStackTrace();
        return;
      }
      tempAPK.deleteOnExit();

      File preloadPath = new File(
          Environment.getExternalStoragePublicDirectory(
              Environment.DIRECTORY_DOWNLOADS
          ),
          "spiggy/" + this.getBranch() + ".js"
      );

      StringBuilder abis = new StringBuilder();
      for (String abi : Build.SUPPORTED_ABIS) {
        if (abis.length() != 0) {
          abis.append("/");
        }
        abis.append(abi);
      }

      NativePatcher.patch(
          Objects.requireNonNull(applicationInfo.publicSourceDir),
          tempAPK.getAbsolutePath(),
          Objects.requireNonNull(this.getPackageName()),
          Objects.requireNonNull(this.getDisplayName()),
          iconPath,
          preloadPath.getAbsolutePath(),
          // Cheaper and easier than messing with unsafe Rust:
          abis.toString()
      );
      System.out.println("Patched APK!");

      KeyStore.PrivateKeyEntry privateKeyEntry;
      try {
        privateKeyEntry = storageController.getSigningKey();
      } catch (Exception e) {
        e.printStackTrace();
        return;
      }
      PrivateKey privateKey = privateKeyEntry.getPrivateKey();
      X509Certificate certificate = (X509Certificate) privateKeyEntry.getCertificate();

      List<X509Certificate> certificates = new ArrayList<>(1);
      certificates.add(certificate);

      ApkSigner.SignerConfig signerConfig = new ApkSigner.SignerConfig.Builder(
          "spiggy", privateKey, certificates
      ).build();
      List<ApkSigner.SignerConfig> signerConfigs = new ArrayList<>(1);
      signerConfigs.add(signerConfig);

      System.out.println("Ready to sign: " + tempAPK.getAbsolutePath());
      ApkSigner signer;
      try {
        signer = new ApkSigner.Builder(signerConfigs)
            .setInputApk(DataSources.asDataSource(new RandomAccessFile(tempAPK, "r")))
            .setOutputApk(tempSignedAPK)
            .build();
      } catch (FileNotFoundException e) {
        e.printStackTrace();
        return;
      }
      try {
        signer.sign();
      } catch (Exception e) {
        e.printStackTrace();
        return;
      }
      tempAPK.delete();

      Uri signedAPKURI;
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        signedAPKURI = FileProvider.getUriForFile(
            spiggyApp,
            BuildConfig.APPLICATION_ID + ".fileprovider",
            tempSignedAPK
        );
      } else {
        signedAPKURI = Uri.fromFile(tempSignedAPK);
      }

      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O &&
          !spiggyApp.getPackageManager().canRequestPackageInstalls()) {
        assert spiggyApp.getActivity() != null;
        spiggyApp.getActivity().startActivity(
          new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES)
            .setData(Uri.parse(String.format("package:%s", spiggyApp.getPackageName())))
        );
      } else {
        installAPK(signedAPKURI);
      }

      System.out.println("We installed!");
    };
    new Thread(runnable).start();
  }

  private void installAPK(Uri signedAPKURI) {
    SpiggyApp spiggyApp = this.storageController.getSpiggyApp();
    Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE)
        .setData(signedAPKURI)
        //.setType("application/vnd.android.package-archive")
        //.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true)
        //.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
    assert spiggyApp.getActivity() != null;
    spiggyApp.getActivity().startActivity(intent);
  }

  public boolean getHasIcon() {
    return this.data.getHasIcon();
  }

  @Nullable
  private ApplicationInfo getDiscordAppInfo() {
    @SuppressLint("QueryPermissionsNeeded") List<ApplicationInfo> packages = storageController.getSpiggyApp()
        .getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);
    for (ApplicationInfo applicationInfo : packages) {
      if (applicationInfo.packageName.equals("com.discord")) {
        return applicationInfo;
      }
    }
    return null;
  }

  public void delete() {
    new Thread(() -> {
      this.storageController.getApps().remove(this);
      this.storageController.syncApps();
      this.getIconPath().delete();
    }).start();
  }
}
