package tech.coolmathgames.spiggy.patcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Utilities {
  static byte[] fullyReadFileToBytes(File f) throws IOException {
    int size = (int) f.length();
    byte[] bytes = new byte[size];
    byte[] tmpBuff = new byte[size];
    try (FileInputStream fis = new FileInputStream(f)) {
      int read = fis.read(bytes, 0, size);
      if (read < size) {
        int remain = size - read;
        while (remain > 0) {
          read = fis.read(tmpBuff, 0, remain);
          System.arraycopy(tmpBuff, 0, bytes, size - remain, read);
          remain -= read;
        }
      }
    } catch (IOException e) {
      throw e;
    }

    return bytes;
  }
}
