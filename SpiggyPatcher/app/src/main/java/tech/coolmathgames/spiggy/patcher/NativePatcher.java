package tech.coolmathgames.spiggy.patcher;

import java.nio.ByteBuffer;

public class NativePatcher {
  static {
    System.loadLibrary("libhostpatcher_jni");
  }

  public static native void patch(
      String originalFile,
      String newFile,
      String packageName,
      String displayName,
      String iconPath,
      String preloadPath,
      String architectures
  );
}
