const util = require("util");

module.exports.hook = function () {
  const tags = {
    debug: 3,
    log: 4,
    warn: 5,
    error: 6,
  };
  let DiscordLogger;

  function lazy() {
    if (!DiscordLogger) {
      DiscordLogger = Java.use("com.discord.app.AppLog")._g.value;
    }
  }

  for (const level of ["debug", "log", "error", "warn"]) {
    const oldLog = console[level] || console.log;
    console[level] = function () {
      lazy();
      const android_log_write = new NativeFunction(
        Module.getExportByName(null, "__android_log_write"),
        "int",
        ["int", "pointer", "pointer"]
      );

      const args = Array.from(arguments)
        .map((arg) => {
          if (typeof arg == "string") {
            return arg;
          } else {
            return util.inspect(arg);
          }
        })
        .join(" ");

      const tag = Memory.allocUtf8String("Spiggy");
      const str = Memory.allocUtf8String(args);

      android_log_write(tags[level], tag, str);

      switch (level) {
        case "log":
          DiscordLogger.i("[spiggy] " + args);
          break;
        case "error":
          DiscordLogger.e("[spiggy] " + args, null, null);
          break;
        case "warn":
          DiscordLogger.w("[spiggy] " + args, null);
          break;
        case "debug":
          DiscordLogger.d("[spiggy] " + args, null);
          break;
        default:
          break;
      }

      return oldLog.apply(console, arguments);
    };
  }
};
