const webpack = require("webpack");
const path = require("path");

const config = {
  entry: "./preload.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "preload.js",
  },
  plugins: [
    new webpack.ProvidePlugin({
      process: "process/browser",
    }),
  ],
  resolve: {
    fallback: {
      util: require.resolve("util/"),
    },
  },
  devtool: false,
  mode: process.env.NODE_ENV == "production" ? "production" : "development",
};

module.exports = config;
