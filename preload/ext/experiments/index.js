module.exports = {
  init: () => {
    Java.perform(function () {
      const Boolean = Java.use("java.lang.Boolean");

      const getExperimentalAlpha = Java.use(
        "com.discord.stores.StoreExperiments$getExperimentalAlpha$1"
      );
      getExperimentalAlpha.invoke.overload().implementation = function (
        ...args
      ) {
        return Boolean.$new(true);
      };

      const View = Java.use("android.view.View");
      const WidgetSettings = Java.use(
        "com.discord.widgets.settings.WidgetSettings"
      );
      WidgetSettings.configureUI.implementation = function (...args) {
        this.configureUI(...args);

        const binding = this.getBinding();
        binding.n.value.setVisibility(View.VISIBLE.value);
        binding.o.value.setVisibility(View.VISIBLE.value);
        binding.m.value.setVisibility(View.VISIBLE.value);
      };
    });
  },
};
